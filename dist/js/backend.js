$( document ).ready(function() {
	
	$('.order__call__popup button[type="submit"]').click(function (event) {
		event.preventDefault();
		$('.order__call__popup').removeClass('active');
		$('.order__call__popup__reminder').addClass('active');
	});

	$('.similar__door__form button[type="submit"]').click(function (event) {
		event.preventDefault();

		$('.similar__door__form__heading, .similar__door__form__content').hide();
		$('.similar__door__reminder').fadeIn();
	});

	$('.leave__request__form button[type="submit"]').click(function (event) {
		event.preventDefault();

		$('.leave__request__popup__bottom__content').hide();
		$('.leave__request__popup__reminder').fadeIn();
	});
});