$( document ).ready(function() {

	$('.btn__mob__menu').click(function () {
		$(this).toggleClass('active');
		$('.header__menu').toggleClass('active');
	});

	$('.header__menu .header__menu__item').click(function () {
		if ($(window).width() < 1141) {
			$('.btn__mob__menu').removeClass('active');
			$('.header__menu').removeClass('active');
		}
	});

	$('.example__block__slider').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  arrows: true,
	  infinite: true
	});

	$('.clients__block__slider').slick({
	  slidesToShow: 6,
	  slidesToScroll: 1,
	  arrows: true,
	  infinite: true,
	  responsive: [
	    {
	      breakpoint: 1199,
	      settings: {
	        slidesToShow: 5
	      }
	    },
	    {
	      breakpoint: 940,
	      settings: {
	        slidesToShow: 4
	      }
	    },
	    {
	      breakpoint: 767,
	      settings: {
	        slidesToShow: 3
	      }
	    },
	    {
	      breakpoint: 640,
	      settings: {
	        slidesToShow: 2
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1
	      }
	    }
	  ]
	});

	$('.leave__request__popup__slider__main').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  arrows: false,
	  fade: true,
	  asNavFor: '.leave__request__popup__slider__nav'
	});

	$('.leave__request__popup__slider__nav').slick({
	  slidesToShow: 4,
	  slidesToScroll: 1,
	  asNavFor: '.leave__request__popup__slider__main',
	  dots: false,
	  centerMode: false,
	  focusOnSelect: true,
	  responsive: [
	    {
	      breakpoint: 1023,
	      settings: {
	        slidesToShow: 3
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 2
	      }
	    }
	  ]
	});

	$(function() {
		var dataDoor = $('.example__block__slider__switch.active').data('door');
		// var dataSlide = $('.example__block__slider__item.slick-current').data('slide');

		$('.similar__door__form button[type="submit"]').attr('data-door', dataDoor);
		// $('.similar__door__form button[type="submit"]').attr('data-slide', dataSlide);

		$(".example__block__slider").slick("slickUnfilter")
		$(".example__block__slider").slick("slickFilter", '[data-door="' + dataDoor + '"]');
		$(".example__block__slider")[0].slick.refresh()
	});

	$('.example__block__slider__switch').click(function () {
		var dataDoor = $(this).data('door');
		// var dataSlide = $(this).data('slide');

		$(".example__block__slider").slick("slickUnfilter")
		$(".example__block__slider").slick("slickFilter", '[data-door="' + dataDoor + '"]');
		$(".example__block__slider")[0].slick.refresh()

		$('.similar__door__form button[type="submit"]').attr('data-door', dataDoor);
		// $('.similar__door__form button[type="submit"]').attr('data-slide', dataSlide);

		if ($(this).hasClass('active')) {

		} else {

			$(this).siblings().removeClass('active');
			$(this).addClass('active');
		}

		if (($(window).width() < 768)) {
			$('html, body').animate({
			      scrollTop: $('.example__block__slider').offset().top - 15 
			  }, 500);
			  return false;
		}
	});

	// $('.example__block__slider').on('afterChange', function(event, slick, currentSlide, nextSlide){
 //  		var dataSlide = $('.example__block__slider__item.slick-current').data('slide');

 //  		$('.similar__door__form button[type="submit"]').attr('data-slide', dataSlide);
	// });

	$('.topLink').click(function () {
	  $('html, body').animate({
	      scrollTop: $($.attr(this, 'href')).offset().top + 15 
	  }, 500);
	  return false;
	});

	$(".popup").overlayScrollbars({ });

	$('input[type="tel"]').inputmask('+7 (999) 999-99-99');

	$('.categories__block__item .btn__order').click(function () {
		var dataDoor = $(this).parents('.categories__block__item').data('door');

		$('.overlay').fadeIn();
		$('.order__call__popup').addClass('active');

		$('.order__call__popup button[type="submit"]').attr('data-door', dataDoor);
	});

	$('.categories__block__item__more, .categories__block__item__heading').click(function () {
		var dataDoor = $(this).parents('.categories__block__item').data('door');
		var itemTitle = $(this).parents('.categories__block__item').children('.categories__block__item__heading').html();
		var itemText = $(this).parents('.categories__block__item').children('.categories__block__item__content').html();
		var itemList = $(this).parents('.categories__block__item').children('.categories__block__item__list__wrap').html();
		var itemSize = $(this).parents('.categories__block__item').children('.categories__block__item__size').html();

		$(".leave__request__popup__slider__main").slick("slickUnfilter")
		$(".leave__request__popup__slider__main").slick("slickFilter", '[data-door="' + dataDoor + '"]');
		$(".leave__request__popup__slider__main")[0].slick.refresh()

		$(".leave__request__popup__slider__nav").slick("slickUnfilter")
		$(".leave__request__popup__slider__nav").slick("slickFilter", '[data-door="' + dataDoor + '"]');
		$(".leave__request__popup__slider__nav")[0].slick.refresh()

		$('.leave__request__popup__title__custom').html(itemTitle);
		$('.leave__request__popup__text').html(itemText);
		$('.leave__request__popup__list__wrap').html(itemList);
		$('.leave__request__popup__size__text').html(itemSize);

		$('.overlay').fadeIn();
		$('.leave__request__popup').addClass('active');
		$('html').addClass('overflow-hidden');

		$('.leave__request__form button[type="submit"]').attr('data-door', dataDoor);
	});

	$('.leave__request__popup .popup__close').click(function () {
		$('html').removeClass('overflow-hidden');
	});

	$('.overlay').click(function () {
		if ($('.leave__request__popup').hasClass('active')) {
			$('html').removeClass('overflow-hidden');
		}

		$('.popup, .popup__reminder').removeClass('active');
		$('.overlay').fadeOut();
	});

	$('.popup__close').click(function () {
		$('.popup, .popup__reminder').removeClass('active');
		$('.overlay').fadeOut();
	});

	$('.get__order__call__popup').click(function () {
		$('.overlay').fadeIn();
		$('.order__call__popup').addClass('active');
	});

	var $animation_elements = $('.work__block__list, .services__block__items__wrap, .advantages__block__list');
	var $window = $(window);

	function check_if_in_view() {
	  var window_height = $window.height();
	  var window_top_position = $window.scrollTop();
	  var window_bottom_position = (window_top_position + window_height);
	 
	  $.each($animation_elements, function() {
	    var $element = $(this);
	    var element_height = $element.outerHeight();
	    var element_top_position = $element.offset().top;
	    var element_bottom_position = (element_top_position + element_height);
	 
	    //check to see if this current container is within viewport
	    if ((element_bottom_position + 10 >= window_top_position) &&
	        (element_top_position <= window_bottom_position)) {
	      $element.addClass('animate');
	    } else {
	      $element.removeClass('animate');
	    }
	  });
	}

	$window.on('scroll', check_if_in_view);
	$window.trigger('scroll');
});